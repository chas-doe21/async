# http_digest.py

import asyncio
from hashlib import md5
import httpx

URLS = [
    "https://duckduckgo.com",
    "https://google.com",
    "https://python.org",
    "https://go.dev",
    "https://www.eff.org",
    "https://www.gnu.org",
    "https://gitlab.com",
    "https://gitlab.com",
]

async def fetch_url(url):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, follow_redirects=True)
    except httpx.ConnectTimeout:
        return url, "timeout"
    except Exception as e:
        return url, e
    if response.status_code != 200:
        return url, f"status code: {response.status_code}"
    return url, response.content

async def main():
    awaitables = [fetch_url(url) for url in URLS]
    # g = await asyncio.gather(*awaitables, return_exceptions=True)
    # for result in g:
    #     if not isinstance(result, Exception):
    #         print(f"{result[0]}: {md5(result[1]).hexdigest()}")
    #     else:
    #         print(result)
    for c in asyncio.as_completed(awaitables):
        result = await c
        if isinstance(result[1], bytes):
            print(f"{result[0]}:\t{md5(result[1]).hexdigest()}")
        else:
            print(f"{result[0]}:\t{result[1]}")

if __name__ == "__main__":
    asyncio.run(main())