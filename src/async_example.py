# async_example.py

import asyncio


async def my_fn(n, name):
    await asyncio.sleep(n)
    print(f"Done with {name}")
    return name


async def main():
    g = await asyncio.gather(my_fn(10, "a"), my_fn(3, "b"))
    print(g)
    for c in asyncio.as_completed((my_fn(10, "a"), my_fn(3, "b"))):
        print(await c)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
